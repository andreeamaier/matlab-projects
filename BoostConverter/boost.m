%Boost Converter

function dx=boost(t,x)
Rds=0.02
RL=0.037;
L=12.e-6;
RC=0.025;
C=22.e-6;
Rld=25;
Vin=18;
Vd=0.4;
F=400000;
D=0.135;

a=mod(t,1/F);
if (a<D/F)
    dx=[1/(L*(Rld+Rds))*x(1)+1/L*Vin;-1/(C*(Rld+RC))*x(2)];
else
    dx=[1/L*Vin-1/L*(RL+RC*Rld/(Rld+RC))*x(1)-1/L*(Rld/(Rld+RC))*x(2)-1/L*Vd;
        Rld/(C*(Rld+RC))*x(1)-1/(C*(Rld+RC))*x(2)];
end
end