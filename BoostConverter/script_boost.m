%Boost Function in order to display the graphic

[t,x]=ode23(@boost, [0 0.001], [0 3])
    subplot(2,1,1);
    plot(x(:,1));
    subplot(2,1,2);
    plot(x(:,2));
   
RC=0.025
C=22.e-6;

Vout=RC*C*diff(x(:,2))+x(1:end-1,2);
    