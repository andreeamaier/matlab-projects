% Buck Converter

function dx=buck(t,x)
Rds=0.02
RL=0.037;
L=12.e-6;
RC=0.025;
C=47.e-6;
Rld=2.5;
Vin=30;
F=400000;
D=0.4;

a=mod(t,1/F);
if (a<D/F)
    dx=[-1/L*(Rds+RL+RC*Rld/(Rld+RC))*x(1)-1/L*(Rld/(Rld+RC))*x(2)+1/L*Vin;
        Rld/(C*(Rld+RC))*x(1)-1/(C*(Rld+RC))*x(2)];
else
    dx=[-1/L*(Rds+RL+RC*Rld/(Rld+RC))*x(1)-1/L*(Rld/(Rld+RC))*x(2);
        Rld/(C*(Rld+RC))*x(1)-1/(C*(Rld+RC))*x(2)];
end
end